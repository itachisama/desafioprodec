# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130830055347) do

  create_table "declarantes", force: true do |t|
    t.string   "nome"
    t.string   "naturalidade"
    t.string   "email"
    t.string   "telefone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "enderecos", force: true do |t|
    t.string   "logradouro"
    t.string   "numero"
    t.string   "complemento"
    t.string   "cep"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ocorrencia", force: true do |t|
    t.datetime "data_ocorrencia"
    t.integer  "declarante_id"
    t.integer  "endereco_id"
    t.string   "detalhes"
    t.string   "tipo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ocorrencia", ["declarante_id"], name: "index_ocorrencia_on_declarante_id"
  add_index "ocorrencia", ["endereco_id"], name: "index_ocorrencia_on_endereco_id"

end
