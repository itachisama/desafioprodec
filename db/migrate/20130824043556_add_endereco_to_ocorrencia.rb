class AddEnderecoToOcorrencia < ActiveRecord::Migration
  def change
  	change_table	:ocorrencia do |t|
  		t.references 	:endereco
  	end
  end
end
