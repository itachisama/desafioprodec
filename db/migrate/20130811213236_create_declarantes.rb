class CreateDeclarantes < ActiveRecord::Migration
  def change
    create_table :declarantes do |t|
      t.string :nome
      t.references :endereco, index: true
      t.string :email
      t.string :naturalidade
      t.string :telefone

      t.timestamps
    end
  end
end
