class CreateDeclarantes < ActiveRecord::Migration
  def change
    create_table :declarantes do |t|
      t.string :nome
      t.string :naturalidade
      t.string :email
      t.string :telefone

      t.timestamps
    end
  end
end
