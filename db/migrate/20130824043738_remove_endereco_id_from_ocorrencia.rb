class RemoveEnderecoIdFromOcorrencia < ActiveRecord::Migration
  def change
  	change_table :ocorrencia do |t|
      t.remove :endereco_id_id
    end
  end
end
