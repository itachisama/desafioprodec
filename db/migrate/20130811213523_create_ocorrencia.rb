class CreateOcorrencia < ActiveRecord::Migration
  def change
    create_table :ocorrencia do |t|
      t.string :tipo
      t.date :data_ocorrencia
      t.time :hora_ocorrencia
      t.references :declarante, index: true
      t.string :detalhes

      t.timestamps
    end
  end
end
