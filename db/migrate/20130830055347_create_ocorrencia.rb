class CreateOcorrencia < ActiveRecord::Migration
  def change
    create_table :ocorrencia do |t|
      t.datetime :data_ocorrencia
      t.references :declarante, index: true
      t.references :endereco, index: true
      t.string :detalhes
      t.string :tipo

      t.timestamps
    end
  end
end
