class RemoveEnderecoFromDeclarates < ActiveRecord::Migration
  def change
    remove_column :declarantes, :endereco_id, :int
  end
end
