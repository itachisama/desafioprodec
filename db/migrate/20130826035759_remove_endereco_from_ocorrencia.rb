class RemoveEnderecoFromOcorrencia < ActiveRecord::Migration
   def change
  	change_table :ocorrencia do |t|
      t.remove :endereco_id
    end
  end
end
