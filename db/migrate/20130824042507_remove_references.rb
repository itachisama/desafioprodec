class RemoveReferences < ActiveRecord::Migration
  def change
  	change_table	:enderecos do |t|
  		t.remove 	:declarante_id
  	end
  end

  def add
  	change_table	:ocorrencias do |t|
  		t.references 	:endereco_id
  	end
  end
end
