class RemoveDeclaranteFromEndereco < ActiveRecord::Migration
  def change
    remove_column :enderecos, :remove_reference, :string
    remove_column :enderecos, :declarante, :string
    remove_column :enderecos, :enderecos, :string
  end
end
