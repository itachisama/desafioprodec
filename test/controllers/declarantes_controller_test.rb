require 'test_helper'

class DeclarantesControllerTest < ActionController::TestCase
  setup do
    @declarante = declarantes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:declarantes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create declarante" do
    assert_difference('Declarante.count') do
      post :create, declarante: { email: @declarante.email, naturalidade: @declarante.naturalidade, nome: @declarante.nome, telefone: @declarante.telefone }
    end

    assert_redirected_to declarante_path(assigns(:declarante))
  end

  test "should show declarante" do
    get :show, id: @declarante
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @declarante
    assert_response :success
  end

  test "should update declarante" do
    patch :update, id: @declarante, declarante: { email: @declarante.email, naturalidade: @declarante.naturalidade, nome: @declarante.nome, telefone: @declarante.telefone }
    assert_redirected_to declarante_path(assigns(:declarante))
  end

  test "should destroy declarante" do
    assert_difference('Declarante.count', -1) do
      delete :destroy, id: @declarante
    end

    assert_redirected_to declarantes_path
  end
end
