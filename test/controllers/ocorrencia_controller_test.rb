require 'test_helper'

class OcorrenciaControllerTest < ActionController::TestCase
  setup do
    @ocorrencium = ocorrencia(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ocorrencia)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ocorrencium" do
    assert_difference('Ocorrencia.count') do
      post :create, ocorrencium: { data_ocorrencia: @ocorrencium.data_ocorrencia, declarante_id: @ocorrencium.declarante_id, detalhes: @ocorrencium.detalhes, endereco_id: @ocorrencium.endereco_id, tipo: @ocorrencium.tipo }
    end

    assert_redirected_to ocorrencium_path(assigns(:ocorrencium))
  end

  test "should show ocorrencium" do
    get :show, id: @ocorrencium
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ocorrencium
    assert_response :success
  end

  test "should update ocorrencium" do
    patch :update, id: @ocorrencium, ocorrencium: { data_ocorrencia: @ocorrencium.data_ocorrencia, declarante_id: @ocorrencium.declarante_id, detalhes: @ocorrencium.detalhes, endereco_id: @ocorrencium.endereco_id, tipo: @ocorrencium.tipo }
    assert_redirected_to ocorrencium_path(assigns(:ocorrencium))
  end

  test "should destroy ocorrencium" do
    assert_difference('Ocorrencia.count', -1) do
      delete :destroy, id: @ocorrencium
    end

    assert_redirected_to ocorrencia_index_path
  end
end
