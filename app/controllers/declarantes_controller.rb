class DeclarantesController < ApplicationController
  before_action :set_declarante, only: [:show, :edit, :update, :destroy]

  # GET /declarantes
  # GET /declarantes.json
  def index
    @declarantes = Declarante.all
  end

  # GET /declarantes/1
  # GET /declarantes/1.json
  def show
  end

  # GET /declarantes/new
  def new
    @declarante = Declarante.new
  end

  # GET /declarantes/1/edit
  def edit
  end

  # POST /declarantes
  # POST /declarantes.json
  def create
    @declarante = Declarante.new(declarante_params)

    respond_to do |format|
      if @declarante.save
        format.html { redirect_to @declarante, notice: 'Declarante was successfully created.' }
        format.json { render action: 'show', status: :created, location: @declarante }
      else
        format.html { render action: 'new' }
        format.json { render json: @declarante.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /declarantes/1
  # PATCH/PUT /declarantes/1.json
  def update
    respond_to do |format|
      if @declarante.update(declarante_params)
        format.html { redirect_to @declarante, notice: 'Declarante was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @declarante.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /declarantes/1
  # DELETE /declarantes/1.json
  def destroy
    @declarante.destroy
    respond_to do |format|
      format.html { redirect_to declarantes_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_declarante
      @declarante = Declarante.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def declarante_params
      params.require(:declarante).permit(:nome, :naturalidade, :email, :telefone)
    end
end
