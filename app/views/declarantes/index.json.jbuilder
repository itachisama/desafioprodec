json.array!(@declarantes) do |declarante|
  json.extract! declarante, :nome, :naturalidade, :email, :telefone
  json.url declarante_url(declarante, format: :json)
end
