json.array!(@ocorrencia) do |ocorrencium|
  json.extract! ocorrencium, :data_ocorrencia, :declarante_id, :endereco_id, :detalhes, :tipo
  json.url ocorrencium_url(ocorrencium, format: :json)
end
