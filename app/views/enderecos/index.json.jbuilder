json.array!(@enderecos) do |endereco|
  json.extract! endereco, :logradouro, :numero, :complemento, :cep
  json.url endereco_url(endereco, format: :json)
end
